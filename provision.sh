#! /bin/bash

set -ex

target_dir=`cd $(dirname ${0}) && pwd`

yum -y install tar
yum -y install make gcc-c++
yum -y install libxml2-devel zlib-devel

cd /usr/local/src
curl -L http://jp2.php.net/get/php-5.6.17.tar.gz/from/this/mirror -o php-5.6.17.tar.gz
tar zxvf php-5.6.17.tar.gz
cd php-5.6.17

./configure \
--prefix=/usr/local \
--enable-mbstring

make && make install

cp ${target_dir}/configs/php.ini /usr/local/lib/php.ini
